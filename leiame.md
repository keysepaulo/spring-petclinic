# Configuração inicial e Clonagem de repositório
```
git clone https://github.com/spring-projects/spring-petclinic.git
```

# Branch principal - main
```
https://gitlab.com/keysepaulo/spring-petclinic/-/tree/main
```

# Branch ramo1
```
https://gitlab.com/keysepaulo/spring-petclinic/-/tree/ramo1
```